# drush/drush

Command line shell for Drupal. https://packagist.org/packages/drush/drush

[![PHPPackages Rank](http://phppackages.org/p/drush/drush/badge/rank.svg)](http://phppackages.org/p/drush/drush)
[![PHPPackages Referenced By](http://phppackages.org/p/drush/drush/badge/referenced-by.svg)](http://phppackages.org/p/drush/drush)

## Official documentation
* [*Using Composer to Install Drupal and Manage Dependencies*
  ](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies)
* [*Quick install for developers (command line)*
  ](https://www.drupal.org/documentation/install/developers)

## Unofficial documentation
* [install drupal with drush](https://google.com/search?q=install+drupal+with+drush)
* [*Installing Drupal with Drush, the Basics*
  ](https://www.valuebound.com/resources/blog/Installing-drupal-with-drush-the-basics)
  2016-02 Jaywant Topno
